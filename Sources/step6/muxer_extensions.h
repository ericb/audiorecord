/*
   This file (muxer_extensions.h) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v2 License.
   Author : Eric Bachard
   Date : 2023/07/15 09:31:15
*/

#ifndef __MUXER_EXTENSIONS_H
#define __MUXER_EXTENSIONS_H

#define EXTENSION_SIZE_MAX  5
#define WORD_SIZE_MAX 256

typedef enum Container_Format {

     MP4_EXT,
     AVI_EXT,
     MATROSKA_EXT,
     WEBM_EXT,
     NOT_A_FORMAT
}  CONTAINER_FORMAT;

#endif /* __MUXER_EXTENSIONS_H */
