#!/bin/bash

# dependencies :
# libavcodec-dev libavdevice-dev libavfilter-dev 
# libavformat-dev libavifiles-dev libavresample-dev libavutil-des
# libopencv-core-dev make libasound g++ libsdl2-dev libswscale-dev
# gdb build-essential

set -x

MY_OS=`uname -s`

DEBUG_LEVEL_VALUE=0
#DEBUG=-DDEBUG_LEVEL=${DEBUG_LEVEL_VALUE}

#DEBUG+=" -ggdb"
#EXTRA_FEATURES=-DDEBUG_CALLBACKS
EXTRA_FEATURES+=" -DDEBUG_EXTENSION=1 -DSDL_ALSA_AUDIO_DEBUG"
#FSANITIZE=-fsanitize-undefined-trap-on-error
#FSANITIZE=-fsanitize=leak
NO_MAIN=-DINTEGRATED_MUXER

FSTACK=-fstack-usage

WARNINGS="-Wall -Wwrite-strings -Wextra "

SECURITY="-Wno-conversion-null -Wuseless-cast -fstack-protector-all "

g++  ${DEBUG} ${NO_MAIN} -D${MY_OS} \
     -std=c++17 ${WARNINGS} ${SECURITY} ${FSANITIZE} ${FSTACK} ${EXTRA_FEATURES} \
     -L/usr/local/lib \
     -o muxer muxer.cpp getch.c \
    `pkg-config --cflags --libs opencv` \
    `pkg-config --cflags --libs libavcodec libavformat libavfilter libswresample libavutil libswresample` \
    `sdl2-config --cflags --libs` \
    -ldl -L/usr/lib  -lpthread -lz -lswscale -lm


