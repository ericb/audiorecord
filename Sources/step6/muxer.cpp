/*
   This file (muxer.cpp) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v2 License.
   Author : Eric Bachard
   Date : 2022/12/31 18:55:30

   A big part of this file comes from muxing.c created by Fabrice Bellard,
   provided inside FFmpeg project documentation.
*/

/*
 * Copyright (c) 2003 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <SDL2/SDL.h>

#include<iostream>
#include <vector>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>

#include "getch.h"

#include "muxer.h"
#include "muxer_extensions.h"

#if DEBUG_LEVEL > 1
static bool isDone = false;
static bool part1  = false;
static bool part2  = false;
#endif

// What follows replaces av_err2str, av_ts2str and av_ts2timestr:

static char a[AV_ERROR_MAX_STRING_SIZE] = { 0 };

// ericb 2022/12/31 ALL of those macros below are broken.
// Using some workaround, adapted from :
// https://www.mail-archive.com/ffmpeg-user@ffmpeg.org/msg30270.html
#undef av_err2str
#define av_err2str(errnum) av_make_error_string( \
        (char*)__builtin_alloca(AV_ERROR_MAX_STRING_SIZE), \
        AV_ERROR_MAX_STRING_SIZE, errnum)

// Same fixes for the other macros causing build breakage
#undef av_ts2str
#define av_ts2str(ts) av_ts_make_string(a, ts)

#undef av_ts2timestr
#define av_ts2timestr(ts,tb) av_ts_make_time_string(a, ts, tb)

////////// AUDIO CALLBACKS STUFF

static SDL_AudioDeviceID input_dev;
static SDL_AudioDeviceID output_dev;

// INVESTIGATE / FIXME : using a ring buffer could lead to some unwaited problems, caused by unpredictable latency.
// See : https://alsa-project.org/wiki/PCM_Ring_Buffer
static Uint8*   audioBuffer = 0;
static int      write_pos   = 0;
static int      read_pos    = 0;
static bool     b_quit      = false;

void cb_in(void *userdata, Uint8 *stream, int len)
{
    //userdata = 0; // could be a parameter passed to the callback
    (void)userdata; // makes the compiler happy

    if (read_pos - write_pos > 0)
    {
        if (len < (read_pos - write_pos))
            len = (read_pos - write_pos);
#if (DEBUG_LEVEL > 1)
        fprintf(stderr, "CASE1 : read_pos - write_pos > 0, read_pos = %d, write_pos = %d et len = %d \n",read_pos, write_pos, len);
#endif
        SDL_memcpy(audioBuffer + write_pos, stream, len);
        write_pos += len;
    }
    else
    {
        if ((write_pos + len) > (AUDIO_BUFFER_SIZE))
        {
            int len1 = AUDIO_BUFFER_SIZE - write_pos;
            int len2 = write_pos + len - AUDIO_BUFFER_SIZE;

#if (DEBUG_LEVEL > 1)
            fprintf(stderr, "CASE2 : (write_pos + len) > (AUDIO_BUFFER_SIZE) read_pos = %d, write_pos = %d\n", read_pos, write_pos );
#endif
            SDL_memcpy(audioBuffer + write_pos, stream, len1);
            SDL_memcpy(audioBuffer, stream, len2);

            write_pos = len2;
        }
        else
        {
#if (DEBUG_LEVEL > 1)
            fprintf(stderr, "CASE3 : last case, means copy all the stream. read_pos = %d, write_pos = %d\n", read_pos, write_pos);
#endif
            SDL_memcpy(audioBuffer + write_pos, stream, len);
            write_pos = write_pos + len;
        }
    }

#ifdef DEBUG_CALLBACKS
    fprintf(stderr, "IN: len = %d\t write_pos =  %d\t stream contains : %d %d %d %d %d %d %d %d\n",
            len, write_pos, stream[0], stream[1], stream[2], stream[3], stream[4], stream[5], stream[6], stream[7]);
#endif
    if (write_pos >= AUDIO_BUFFER_SIZE)
        write_pos = write_pos - AUDIO_BUFFER_SIZE;

    // Make sure the thread completes when application quit,
    // to avoid leaking ressources
    if (b_quit == true)
        return;
}

void cb_out(void *userdata, Uint8 *stream, int len)
{
    //userdata = 0; // could be a parameter passed to the callback
    (void)userdata; // makes the compiler happy

    if (read_pos > write_pos)
    {
        if ((read_pos + len) > AUDIO_BUFFER_SIZE)
        {
            // read the end of the audioBuffer
            int len5 = AUDIO_BUFFER_SIZE - read_pos;
            SDL_memcpy(stream, audioBuffer + read_pos, len5);

            // + the beginning
            int len6 = (read_pos + len) - AUDIO_BUFFER_SIZE;
            SDL_memcpy(stream, audioBuffer, len6);

            read_pos = len6;
        }
        else
        {
            SDL_memcpy(stream, audioBuffer + read_pos, len);
            read_pos += len;
        }
    }
    else if (read_pos + len > write_pos)
    {
        SDL_memcpy(stream, audioBuffer + read_pos, write_pos - read_pos);
        read_pos += (write_pos - read_pos);
    }
    else
    {
        SDL_memcpy(stream, audioBuffer + read_pos, len);
        read_pos += len;
    }
#ifdef DEBUG_CALLBACKS
    // IN and OUT MUST match
    fprintf(stderr, "OUT: len = %d\t  read_pos =  %d\t stream contains : %d %d %d %d %d %d %d %d\n",
            len, read_pos, stream[0], stream[1], stream[2], stream[3], stream[4], stream[5], stream[6], stream[7]);
#endif

    if ((read_pos + len) > AUDIO_BUFFER_SIZE)
        read_pos = (read_pos + len) - AUDIO_BUFFER_SIZE;

    // Make sure the thread completes when application quit,
    // to avoid leaking ressources
    if (b_quit == true)
        return;
}
////////// END AUDIO CALLBACKS STUFF



////////// OpenCV stuff
// we receive cv::Mat from OpenCV API, and we convert it into AVFrame (of video type)
static cv::Mat image;
static cv::VideoCapture * p_cvcap;
////////// END OpenCV stuff


////////// FFMPEG API starts below

// a wrapper around a single output AVStream
typedef struct OutputStream
{
    AVStream *st;
    AVCodecContext *enc;

    // pts of the next frame that will be generated
    int64_t next_pts;
    int samples_count;

    AVFrame *frame;
    AVFrame *tmp_frame;

    float t;
    float tincr;

    struct SwsContext *sws_ctx;
    struct SwrContext *swr_ctx;
} OutputStream;


// declaration forward. Create md::muxer class instead ?
#if (DEBUG_LEVEL > 1)
static void log_packet(const AVFormatContext *, const AVPacket *);
#endif
static void close_stream(OutputStream *);
static int write_frame(AVFormatContext *, AVCodecContext *, AVStream *, AVFrame *);
static void add_stream(OutputStream *, AVFormatContext *, AVCodec **, enum AVCodecID);

// AUDIO FRAME
static AVFrame *alloc_audio_frame(enum AVSampleFormat, uint64_t, int, int);
static AVFrame *get_audio_frame(OutputStream *);
static int write_audio_frame(AVFormatContext *, OutputStream *);
static void open_audio(AVCodec *, OutputStream *, AVDictionary *);

// VIDEO FRAME
static AVFrame *alloc_picture(enum AVPixelFormat, int, int);
static void open_video(AVCodec *, OutputStream *, AVDictionary *);
static AVFrame *get_video_frame(OutputStream *);
static int write_video_frame(AVFormatContext *, OutputStream *);

static const AVRational dst_fps = { VIDEO_STREAM_FRAME_RATE, 1 };

// implementation

#if (DEBUG_LEVEL > 1)
static void find_extension(void);

static void log_packet(const AVFormatContext *fmt_ctx,
                       const AVPacket *pkt)
{
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;

    if ((!isDone) && (!part2))
    {
        fprintf(stderr, "pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
               av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
               av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
               av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base),
               pkt->stream_index);
        part2 = true;
        isDone = part1 && part2;
    }
}
#endif

static void close_stream(OutputStream *ost)
{
    avcodec_free_context(&ost->enc);
    av_frame_free(&ost->frame);
    av_frame_free(&ost->tmp_frame);
    sws_freeContext(ost->sws_ctx);
    swr_free(&ost->swr_ctx);
}

static int write_frame(AVFormatContext *fmt_ctx,
                       AVCodecContext *c,
                       AVStream *st,
                       AVFrame *frame)
{
    // send the frame to the encoder
    int ret = avcodec_send_frame(c, frame);

    if (ret < 0)
    {
        fprintf(stderr, "Error sending a frame to the encoder: %s\n",
                av_err2str(ret));
        exit(1);
    }

    while (ret >= 0)
    {
        // fill the structure with zeros to properly initialize it
        AVPacket pkt = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ret = avcodec_receive_packet(c, &pkt);

        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            break;
        else if (ret < 0)
        {
            fprintf(stderr, "Error encoding a frame: %s\n", av_make_error_string(a, AV_ERROR_MAX_STRING_SIZE, ret));
            exit(1);
        }

        // rescale output packet timestamp values from codec to stream timebase
        av_packet_rescale_ts(&pkt, c->time_base, st->time_base);
        pkt.stream_index = st->index;

#if (DEBUG_LEVEL > 1)
        // Write the compressed frame to the media file.
        log_packet(fmt_ctx, &pkt);
#endif
        // FIXME : do we really need interleaved frames ?
        ret = av_interleaved_write_frame(fmt_ctx, &pkt);
        av_packet_unref(&pkt);

        if (ret < 0)
        {
            fprintf(stderr, "Error while writing output packet: %s\n", av_make_error_string(a, AV_ERROR_MAX_STRING_SIZE, ret));
            exit(1);
        }
    }
    return (ret == AVERROR_EOF ? 1 : 0);
}


static void add_stream(OutputStream *ost, AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id)
{
    int i = 0;
    AVCodecContext *c;

    // find the encoder
    *codec = avcodec_find_encoder(codec_id);

    if (!(*codec))
    {
        fprintf(stderr, "Could not find encoder for '%s'\n", avcodec_get_name(codec_id));
        exit(1);
    }
    else
        fprintf(stderr, "Found encoder : '%s'\n", avcodec_get_name(codec_id));

    ost->st = avformat_new_stream(oc, NULL);

    if (!ost->st)
    {
        fprintf(stderr, "Could not allocate stream\n");
        exit(1);
    }

    ost->st->id = oc->nb_streams-1;
    c = avcodec_alloc_context3(*codec);

    if (!c)
    {
        fprintf(stderr, "Could not alloc an encoding context\n");
        exit(1);
    }
    ost->enc = c;

    switch ((*codec)->type)
    {
        // https://stackoverflow.com/questions/21342194/sdl2-ffmpeg2-intermittent-clicks-instead-of-audio
        // https://stackoverflow.com/questions/32701507/what-is-the-best-way-to-fill-avframe-data
        // https://stackoverflow.com/questions/27439123/how-to-fill-avframe-with-audio-data

        case AVMEDIA_TYPE_AUDIO:
#if DEBUG_LEVEL > 1
            fprintf(stderr, "Checking AV_SAMPLE_FORMAT ... \n");
            fprintf(stderr, "Reminder :. \n");
            fprintf(stderr,"AV_SAMPLE_FMT_S16 = %d \n", AV_SAMPLE_FMT_S16);
            fprintf(stderr,"AV_SAMPLE_FMT_S32 = %d \n", AV_SAMPLE_FMT_S32);
            fprintf(stderr,"AV_SAMPLE_FMT_FLT = %d \n", AV_SAMPLE_FMT_FLT);
            fprintf(stderr,"AV_SAMPLE_FMT_S16P = %d \n", AV_SAMPLE_FMT_S16P);
            fprintf(stderr,"AV_SAMPLE_FMT_S32P = %d \n", AV_SAMPLE_FMT_S32P);
            fprintf(stderr,"AV_SAMPLE_FMT_FLTP = %d \n", AV_SAMPLE_FMT_FLTP);
            fprintf(stderr,"AV_SAMPLE_FMT_S64 = %d \n", AV_SAMPLE_FMT_S64);
            fprintf(stderr,"AV_SAMPLE_FMT_S64P = %d \n", AV_SAMPLE_FMT_S64P);
            fprintf(stderr,"(*codec)->sample_fmts[0] contains :  = %d \n", (*codec)->sample_fmts[0]);
            fprintf(stderr,"(*codec)->sample_fmts[1] contains :  = %d \n", (*codec)->sample_fmts[1]);
            fprintf(stderr,"(*codec)->sample_fmts[2] contains :  = %d \n", (*codec)->sample_fmts[2]);
            fprintf(stderr,"(*codec)->sample_fmts[3] contains :  = %d \n", (*codec)->sample_fmts[3]);
#endif
            c->sample_fmt  = (*codec)->sample_fmts[0];

            // FIXME : figure out why fltp is mandatory ...
            // HACK to allow .avi and .webm recording
            if ((c->sample_fmt == AV_SAMPLE_FMT_S32P))
                c->sample_fmt = AV_SAMPLE_FMT_FLTP;

            if ((c->sample_fmt == AV_SAMPLE_FMT_S16)||(c->sample_fmt == AV_SAMPLE_FMT_FLT))
                c->sample_fmt = AV_SAMPLE_FMT_FLTP;

            c->bit_rate    = ost->enc->bit_rate;//AUDIO_BIT_RATE;//64000 or 192000 ? (more usual value)
            c->sample_rate = ost->enc->sample_rate;//DEFAULT_SAMPLE_RATE;//44100 or 48000

            if ((*codec)->supported_samplerates)
            {
                fprintf(stderr,"(*codec)->supported_samplerates[0] = %d \n", (*codec)->supported_samplerates[0]);
                fprintf(stderr,"(*codec)->supported_samplerates[1] = %d \n", (*codec)->supported_samplerates[1]);
                fprintf(stderr,"(*codec)->supported_samplerates[2] = %d \n", (*codec)->supported_samplerates[2]);
                fprintf(stderr,"(*codec)->supported_samplerates[3] = %d \n", (*codec)->supported_samplerates[3]);
                fprintf(stderr,"(*codec)->supported_samplerates[4] = %d \n", (*codec)->supported_samplerates[4]);
                fprintf(stderr,"(*codec)->supported_samplerates[5] = %d \n", (*codec)->supported_samplerates[5]);
                fprintf(stderr,"(*codec)->supported_samplerates[6] = %d \n", (*codec)->supported_samplerates[6]);

                c->sample_rate = (*codec)->supported_samplerates[0];

                fprintf(stderr,"We got : c->sample_rate = %d \n", c->sample_rate);
            }

            c->channels        = av_get_channel_layout_nb_channels(c->channel_layout);
            c->channel_layout = AV_CH_LAYOUT_STEREO;

            if ((*codec)->channel_layouts)
            {
                c->channel_layout = (*codec)->channel_layouts[0];

                for (i = 0; (*codec)->channel_layouts[i]; i++)
                {
                    if ((*codec)->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
                        c->channel_layout = AV_CH_LAYOUT_STEREO;
                }
            }

            c->channels        = av_get_channel_layout_nb_channels(c->channel_layout);
            ost->st->time_base = (AVRational){ 1, c->sample_rate };

            std::cerr <<  " ost->st->time_base = " <<  ost->st->time_base.num << "/" << ost->st->time_base.den   << "\n";

            fprintf(stderr, "Found AV sample_fmt of type :  %d, means : %s\n", c->sample_fmt, av_get_sample_fmt_name(c->sample_fmt));
            fprintf(stderr, "this sample format has %d bytes per sample , \n", av_get_bytes_per_sample(c->sample_fmt));
            fprintf(stderr, "and its buffer size is equal to %d \n",
                    av_samples_get_buffer_size(NULL, c->channels, AUDIO_SAMPLES_NUMBER, c->sample_fmt, 0));

            break;

        case AVMEDIA_TYPE_VIDEO:
            c->codec_id = codec_id;
            // c->bit_rate = 400000;// too low, leads to bad quality images
            // over 2000000, the images are nice, but cpu overload can occur
            // Last, a value between 3000000 and 4000000 seems to be an optimal value,
            // including with low light ambiance
            c->bit_rate = VIDEO_BIT_RATE;

            // Resolution must be a multiple of two.
            c->width    = FRAME_WIDTH;
            c->height   = FRAME_HEIGHT;

            // timebase: This is the fundamental unit of time (in seconds) in terms
            // of which frame timestamps are represented. For fixed-fps content,
            // timebase should be 1/framerate and timestamp increments should be
            // identical to 1.
            ost->st->time_base = (AVRational){ 1, VIDEO_STREAM_FRAME_RATE };
            c->time_base       = ost->st->time_base;

            //c->framerate = dst_fps;
            c->gop_size      = 12; // emit one intra frame every twelve frames at most
            c->pix_fmt       = STREAM_PIX_FMT;

            if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
                // just for testing, we also add B-frames
                c->max_b_frames = 2;
            }

            if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO)
            {
                // Needed to avoid using macroblocks in which some coeffs overflow.
                // This does not happen with normal video, it just happens here as
                // the motion of the chroma plane does not match the luma plane.
                c->mb_decision = 2;
            }
            break;

        default:
            break;
    }

    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
}


/////////////////  AUDIO  /////////////////

static AVFrame * alloc_audio_frame(enum AVSampleFormat sample_fmt,
                                   uint64_t channel_layout,int sample_rate, int nb_samples)
{
    AVFrame *frame = av_frame_alloc();
    int ret;

    if (!frame)
    {
        fprintf(stderr, "Error allocating an audio frame\n");
        exit(1);
    }

    frame->format = sample_fmt;
    frame->channel_layout = channel_layout;
    frame->sample_rate = sample_rate;
    frame->nb_samples = nb_samples;

    if (nb_samples)
    {
        ret = av_frame_get_buffer(frame, 0);

        if (ret < 0)
        {
            fprintf(stderr, "Error allocating an audio buffer\n");
            exit(1);
        }
    }
    return frame;
}


/* Prepare a 16 (or 8 ?) bits dummy audio frame of 'frame_size' samples and 'nb_channels' channels. */
static AVFrame *get_audio_frame(OutputStream *ost)
{
    if (b_quit == true)
        return NULL;

    AVFrame *frame = ost->tmp_frame;
    int current_read_pos = read_pos;

    // Needed ?  
    //SDL_MixAudioFormat(stream, (const Uint8 *)audioBuffer.readPoint, AUDIO_S16SYS, len, SDL_MIX_MAXVOLUME);
    int16_t *q = (int16_t*)frame->data[0];

    /* check if we want to generate more frames */
    if (av_compare_ts(ost->next_pts, ost->enc->time_base, STREAM_DURATION, (AVRational){ 1, 1 }) > 0)
    {
        // STREAM_DURATION value reached
        b_quit = true;
        return NULL;
    }


    for (int j = 0; j < frame->nb_samples; j++)
    {
        // make sure the frame is writable -- makes a copy if the encoder kept a reference internally
        int ret = av_frame_make_writable(frame);

        if (ret < 0)
            exit(1);

        //Why mono ?        for (int k = 0; k < ost->enc->channels; k++)
        {
            q[2*j]   = VOLUME_AMPLIFICATION_FACTOR * audioBuffer[current_read_pos + j];
        }

        ost->t += ost->tincr;
    }

    frame->pts     = ost->next_pts;
    ost->next_pts += frame->nb_samples;

    return frame;
}

static int write_audio_frame(AVFormatContext *oc, OutputStream *ost)
{
    AVCodecContext *c;
    AVFrame *frame;
    int ret;
    int dst_nb_samples;

    c = ost->enc;
    frame = get_audio_frame(ost);

    if (frame)
    {
        /* convert samples from native format to destination codec format, using the resampler */
        /* compute destination number of samples */
        dst_nb_samples = av_rescale_rnd(swr_get_delay(ost->swr_ctx, c->sample_rate) + frame->nb_samples,
                                        c->sample_rate, c->sample_rate, AV_ROUND_UP);
        av_assert0(dst_nb_samples == frame->nb_samples);

        /*
         * when we pass a frame to the encoder, it may keep a reference to it internally;
         * make sure we do not overwrite it here
         */
        ret = av_frame_make_writable(ost->frame);

        if (ret < 0)
            exit(1);

        /* convert to destination format */
        ret = swr_convert(ost->swr_ctx,
                          ost->frame->data, dst_nb_samples,
                          (const uint8_t **)frame->data, frame->nb_samples);
        if (ret < 0)
        {
            fprintf(stderr, "Error while converting\n");
            exit(1);
        }
        frame = ost->frame;
        frame->pts = av_rescale_q(ost->samples_count, (AVRational){1, c->sample_rate}, c->time_base);
        ost->samples_count += dst_nb_samples;
    }

    return write_frame(oc, c, ost->st, frame);
}

static void open_audio(AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg)
{
    AVCodecContext *c;
    int nb_samples;
    int ret;
    AVDictionary *opt = NULL;
    c = ost->enc;

    /* open it */
    av_dict_copy(&opt, opt_arg, 0);
    ret = avcodec_open2(c, codec, &opt);

    // add dictionary options here//
    av_dict_free(&opt);

    if (ret < 0)
    {
        fprintf(stderr, "Could not open audio codec, and ret =  %d\n", ret);
        exit(1);
    }

    /* init signal generator */
    ost->t     = 0;
    ost->tincr = 1 / c->sample_rate;
    nb_samples = c->frame_size;

    fprintf(stderr, "nb_samples = %d \n", nb_samples);

    ost->frame     = alloc_audio_frame(c->sample_fmt, c->channel_layout,
                                       c->sample_rate, nb_samples);

    ost->tmp_frame = alloc_audio_frame(AV_SAMPLE_FMT_S16, c->channel_layout,
                                       c->sample_rate, nb_samples);

    /* copy the stream parameters to the muxer */
    ret = avcodec_parameters_from_context(ost->st->codecpar, c);

    if (ret < 0)
    {
        fprintf(stderr, "Could not copy the stream parameters\n");
        exit(1);
    }

    /* create resampler context */
    ost->swr_ctx = swr_alloc();

    if (!ost->swr_ctx) {
        fprintf(stderr, "Could not allocate resampler context\n");
        exit(1);
    }

    /* set options */
    // FIXME : missing options ?
    av_opt_set_int       (ost->swr_ctx, "in_channel_count",   c->channels,       0);
    av_opt_set_int       (ost->swr_ctx, "in_sample_rate",     c->sample_rate,    0);
    av_opt_set_sample_fmt(ost->swr_ctx, "in_sample_fmt",      AV_SAMPLE_FMT_S16, 0);
    av_opt_set_int       (ost->swr_ctx, "out_channel_count",  c->channels,       0);
    av_opt_set_int       (ost->swr_ctx, "out_sample_rate",    c->sample_rate,    0);
    av_opt_set_sample_fmt(ost->swr_ctx, "out_sample_fmt",     c->sample_fmt,     0);

    /* initialize the resampling context */
    if ((ret = swr_init(ost->swr_ctx)) < 0)
    {
        fprintf(stderr, "Failed to initialize the resampling context\n");
        exit(1);
    }
}
/////////////////  END AUDIO  ////////////////////////


/////////////////  VIDEO  ////////////////////////

static AVFrame *alloc_picture(enum AVPixelFormat pix_fmt, int width, int height)
{
    // the AVFrame to be returned
    AVFrame *avframe;

    int ret;
    avframe = av_frame_alloc();

    if (!avframe)
        return NULL;

    // minimal need for an AVFrame
    avframe->format = pix_fmt;
    avframe->width  = width;
    avframe->height = height;

    /* allocate the buffers for the frame data */
    ret = av_frame_get_buffer(avframe, 0);

    if (ret < 0)
    {
        fprintf(stderr, "Could not allocate frame data.\n");
        exit(1);
    }

    return avframe;
}


static void open_video(AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg)
{
    int ret = 0;
    AVCodecContext *c = ost->enc;
    AVDictionary *opt = NULL;

    av_dict_set(&opt, "movflags", "faststart", 0 ); // FIXME : only for H264 (moov atom not found)
    av_dict_copy(&opt, opt_arg, 0);

    /* open the codec */
    ret = avcodec_open2(c, codec, &opt);
    av_dict_free(&opt);

    if (ret < 0)
    {
        // is it g++ bug or crappy macro ?
        // SEE : https://stackoverflow.com/questions/32941846/c-error-taking-address-of-temporary-array
        fprintf(stderr, "Could not open video codec: %s\n", av_err2str(ret));
        exit(1);
    }

    /* allocate and init a re-usable frame */
    ost->frame = alloc_picture(c->pix_fmt, c->width, c->height);

    if (!ost->frame)
    {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }

    // If the output format is not YUV420P, then a temporary YUV420P picture is needed too.
    ost->tmp_frame = alloc_picture(c->pix_fmt, c->width, c->height);

    if (!ost->tmp_frame)
    {
        fprintf(stderr, "Could not allocate temporary picture\n");
        exit(1);
    }

    /* copy the stream parameters to the muxer */
    ret = avcodec_parameters_from_context(ost->st->codecpar, c);

    if (ret < 0)
    {
        fprintf(stderr, "Could not copy the stream parameters\n");
        exit(1);
    }
}

// helper function to check for FFmpeg errors
inline void checkError(int error, const std::string &message)
{
    if (error < 0)
    {
        std::cerr << message << ": " << av_err2str(error) << std::endl;
        exit(EXIT_FAILURE);
    }
}

static AVFrame *get_video_frame(OutputStream *ost)
{
    if (b_quit == true)
        return NULL;

    AVFrame * frame = ost->frame;
    AVCodecContext *c = ost->enc;
    static bool b_cvMat_available = false;

    int cvLinesizes[1];

    /* check if we want to generate more frames */
    // FIXME : find another way to quit and create the mov atom (and make the video valid)
    // something like return NULL when a key is hit
    if (av_compare_ts(ost->next_pts, c->time_base,
                      STREAM_DURATION, (AVRational){ 1, 1 }) > 0)
        return NULL;

    if (av_frame_make_writable(ost->frame) < 0)
        exit(1);

    SwsContext* swsctx = sws_getCachedContext(
            nullptr, c->width, c->height, AV_PIX_FMT_BGR24,
            c->width, c->height, c->pix_fmt, SWS_BICUBIC, nullptr, nullptr, nullptr);

    if (!swsctx)
    {
        std::cerr << "fail to sws_getCachedContext";
        return 0;
    }

    ost->tmp_frame->width = c->width;
    ost->tmp_frame->height = c->height;
    ost->tmp_frame->format = static_cast<int>(c->pix_fmt);

    b_cvMat_available = p_cvcap->read(image);

    if (!b_cvMat_available)
        std::cout << "Pb reading capture device" << "\n";

    cvLinesizes[0] = image.step1();

    av_image_alloc( ost->tmp_frame->data,
                    ost->tmp_frame->linesize,
                    c->width,
                    c->height,
                    AVPixelFormat::AV_PIX_FMT_YUV420P,
                    1
                  );

    SwsContext *conversion = sws_getContext(
          c->width, c->height, AVPixelFormat::AV_PIX_FMT_BGR24, c->width, c->height,
          (AVPixelFormat)frame->format, SWS_FAST_BILINEAR, NULL, NULL, NULL);

    sws_scale(conversion, &image.data, cvLinesizes, 0, c->height, frame->data,
              frame->linesize);

    sws_freeContext(conversion);
    ost->frame->pts = ost->next_pts++;

    return frame;
}

/*
 * encode one video frame and send it to the muxer
 * return 1 when encoding is finished, 0 otherwise
 */
static int write_video_frame(AVFormatContext *oc, OutputStream *ost)
{
    return write_frame(oc, ost->enc, ost->st, get_video_frame(ost));
}

static void keyboard_loop()
{
    char c = 0;
    while ((c != 27) && (c != 20)) // esc
    {
        c = 0;
        c=getch();
    }

    if ((c == 27)|| (c == 20)) // esc
        b_quit = true;
}
////////// END FFMPEG API

static  char extension[EXTENSION_SIZE_MAX]="avi"; // default

#if (DEBUG_LEVEL > 1)
static void find_extension(void)
{
    CONTAINER_FORMAT container_fmt = NOT_A_FORMAT;

    fprintf(stderr,"extension of type : %s \n", extension);

    if(!strcmp(extension, "mp4"))
        container_fmt = MP4_EXT;
    else if(!strcmp(extension, "avi"))
        container_fmt = AVI_EXT;
    else if(!strcmp(extension, "mkv"))
        container_fmt = MATROSKA_EXT;
    else if(!strcmp(extension, "webm"))
        container_fmt = WEBM_EXT;

    switch(container_fmt)
    {
        case MP4_EXT:
            fprintf(stderr, "Recording using mp4 container\n");
        break;

        case AVI_EXT:
            fprintf(stderr, "Recording using avi container\n");
        break;

        case MATROSKA_EXT:
            fprintf(stderr, "Recording using matroska (alias mkv) container\n");
        break;

        case WEBM_EXT:
            fprintf(stderr, "Recording using webm container\n");
        break;

        default:
            fprintf(stderr, "Recording using not recommanded container type\n");
        break;
    }
}
#endif


int MAIN(int argc, char **argv)
{
    OutputStream video_st = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; 
    OutputStream audio_st = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    AVOutputFormat *fmt;


    AVCodec *audio_codec, *video_codec;
    int ret = 0;
    int have_video = 0, have_audio = 0;
    int encode_video = 0, encode_audio = 0;
    AVDictionary *opt = NULL;

    if (argc < 2)
    {
        std::cout << "Usage: record_and_save_videos <outfile>" << std::endl;
        return 1;
    }

    // Retrieve the extension name

    // First, keep the full filename in outfile,
    // because argv[1] will be truncated by strtok(), in order to retrieve the extension name
    std::string outfile = argv[1];
    char * motTemp = argv[1];
    motTemp = strtok(motTemp,".");
    motTemp = strtok(NULL,"");

    strncpy(extension,motTemp,EXTENSION_SIZE_MAX);

#if (DEBUG_LEVEL > 1)
    int filename_length = strlen(outfile.c_str());

    find_extension();
    fprintf(stderr, "argv[1] contains : %s after being used by strtok.\nThe final file name is %s, and it contains %d characters\n",
                                        argv[1], outfile.c_str(), filename_length);
#endif
    //  av_log_set_level(AV_LOG_DEBUG);

    // initialize OpenCV capture as input frame generator
#if defined (Linux)
    //putenv((char *)"SDL_AUDIODRIVER=alsa");
    putenv((char *)"SDL_AUDIODRIVER=alsa");
    putenv((char *)"SDL_AUDIO_ALSA_DEBUG=TRUE");

    cv::VideoCapture cvcap(DEFAULT_VIDEO_DEVICE + cv::CAP_V4L2);
#elif defined(_WIN32)
    putenv("SDL_AUDIODRIVER=DirectSound");

    cv::VideoCapture cvcap(DEFAULT_VIDEO_DEVICE + cv::CAP_DSHOW);
#endif
    p_cvcap = &cvcap;

    cv::Mat image(FRAME_HEIGHT, FRAME_WIDTH, CV_8UC3);

    if (!cvcap.isOpened())
    {
        std::cerr << "fail to open cv::VideoCapture";
        return 2;
    }

    cvcap.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    cvcap.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
    cvcap.set(cv::CAP_PROP_FOURCC, (CV_FOURCC('M','J','P','G')));
    cvcap.set(cv::CAP_PROP_FPS, VIDEO_STREAM_FRAME_RATE);

    std::cerr << "FPS : " << p_cvcap->get(cv::CAP_PROP_FPS) << "\n";


    // open output format context
    AVFormatContext* outctx = nullptr;

    // NOTATIONS : dans muxing.c outtcx s'appelle oc  et outfile s'appelle filename
    ret = avformat_alloc_output_context2(&outctx, nullptr, nullptr, outfile.c_str());

    if (!outctx) /* or equivalent : if (ret < 0) */
    {
        fprintf(stderr, "Could not deduce output format from file extension: using MPEG.\n");
        avformat_alloc_output_context2(&outctx, NULL, "mpeg", outfile.c_str());
        std::cerr << " ret = " << ret << "\n";
        return 2;
    }

    fmt = outctx->oformat;

    /* Add the audio and video streams using the default format codecs and initialize the codecs. */
    if (fmt->video_codec != AV_CODEC_ID_NONE)
    {
        add_stream(&video_st, outctx, &video_codec, fmt->video_codec);
        have_video = 1;
        encode_video = 1;
    }
    if (fmt->audio_codec != AV_CODEC_ID_NONE) {
        add_stream(&audio_st, outctx, &audio_codec, fmt->audio_codec);
        have_audio = 1;
        encode_audio = 1;
    }

    // audio : create SDL callbacks
    SDL_Init(SDL_INIT_AUDIO);
    SDL_Log("Using audio driver: %s\n", SDL_GetCurrentAudioDriver());

    audioBuffer = (uint8_t *)malloc(AUDIO_BUFFER_SIZE);

    SDL_AudioSpec want_in, want_out, have_in, have_out;
    SDL_zero(want_out);
    want_out.freq = audio_st.enc->sample_rate;//DEFAULT_SAMPLE_RATE;//48000;//
    want_out.format = audio_st.enc->sample_fmt ;// SDL_AUDIO_FORMAT;// AUDIO_S16SYS; //AUDIO_F32;//AUDIO_S16LSB;//AUDIO_S16;//AUDIO_S32;// 

/// WHY mono only ?
    want_out.channels = 1;//audio_st.enc->channels ;//CHANNELS;

    want_out.samples = AUDIO_SAMPLES_NUMBER;//audio_st.enc->frame_size;// 1024;
    want_out.callback = cb_out;

    fprintf(stderr, "Adresse de cb_out =  %p  \n", &cb_out);

    output_dev = SDL_OpenAudioDevice(NULL /* default */, 0 /* isCapture*/ , &want_out, &have_out, SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (output_dev == 0)
    {
        SDL_Log("Failed to open output: %s", SDL_GetError());
        return 1;
    }

    SDL_zero(want_in);
    want_in.freq = audio_st.enc->sample_rate;//DEFAULT_SAMPLE_RATE;//48000;
    want_in.format = audio_st.enc->sample_fmt;//SDL_AUDIO_FORMAT;
    want_in.channels = 1;//audio_st.enc->channels ;//CHANNELS;
    want_in.samples = AUDIO_SAMPLES_NUMBER;//audio_st.enc->frame_size;//1024;
    want_in.callback = cb_in;
#if DEBUG_LEVEL > 1
    fprintf(stderr, "Adresse de cb_in =  %p  \n", &cb_in);
    fprintf(stderr, "have_out.freq    =  %d  \n", have_out.freq);
    fprintf(stderr, "have_out.samples =  %d  \n", have_out.samples);
#endif

    input_dev = SDL_OpenAudioDevice(NULL /* default */, 1 /* isCapture */, &want_in, &have_in, SDL_AUDIO_ALLOW_ANY_CHANGE);

    fprintf(stderr, "[SDL] Obtained - have.out.freq : %d Audio format: f %d Is audio signed : s %d \n \
    Is audio big endian : %d Audio bits size : %d Number of channels: %d have.out samples = %d\n",
                    have_out.freq, SDL_AUDIO_ISFLOAT(have_out.format),
                    SDL_AUDIO_ISSIGNED(have_out.format), SDL_AUDIO_ISBIGENDIAN(have_out.format),
                    SDL_AUDIO_BITSIZE(have_out.format), have_out.channels, have_out.samples);

    if (input_dev == 0) {
        SDL_Log("Failed to open input: %s", SDL_GetError());
        return 1;
    }
    // audio callbacks done

    /* Now that all the parameters are set, we can open the audio and
     * video codecs and allocate the necessary encode buffers. */
    if (have_video)
        open_video(video_codec, &video_st, opt);

    if (have_audio)
        open_audio(audio_codec, &audio_st, opt);

    av_dump_format(outctx, 0, outfile.c_str(), 1);

    /* open the output file, if needed */
    if (!(fmt->flags & AVFMT_NOFILE))
    {
        ret = avio_open(&outctx->pb, outfile.c_str(), AVIO_FLAG_WRITE);

        if (ret < 0)
        {
            fprintf(stderr, "Could not open '%s': %s\n", outfile.c_str(), av_err2str(ret));
            return 1;
        }
    }

    /* Write the stream header, if any. */
    ret = avformat_write_header(outctx, &opt);

    if (ret < 0)
    {
        fprintf(stderr, "Error occurred when opening %s, and the error is %s\n", outfile.c_str(), av_err2str(ret));
        return 1;
    }

    // start audio  recording / playback
    SDL_PauseAudioDevice(input_dev, 0);
    SDL_PauseAudioDevice(output_dev, 0);

    // lock the loop
    std ::thread first(keyboard_loop);

    // IMPORTANT : the webcam continues to read/write audio data
    // until the ESC key is hit AND CTRL+C does not stop the loop !

    while ((encode_video || encode_audio) && (!b_quit))
    {
        /* select the stream to encode */
        if (encode_video &&
            (!encode_audio || av_compare_ts(video_st.next_pts, video_st.enc->time_base,
                                            audio_st.next_pts, audio_st.enc->time_base) <= 0))
        {
            encode_video = !write_video_frame(outctx, &video_st);
        }
        else
            encode_audio = !write_audio_frame(outctx, &audio_st);
    }

    // To avoid the "2 frames left in the queue on closing" log,
    // send an extra empty/NULL audio frame, to make sure anything queued will be flushed
    if (b_quit == true)
        encode_audio = !write_audio_frame(outctx, &audio_st);

    /* Write the trailer, if any. The trailer must be written before you close the CodecContexts
     * open when you wrote the header; otherwise av_write_trailer() may try to use memory that 
     * was freed on av_codec_close(). */
    av_write_trailer(outctx);

    /* Close each codec. */
    if (have_video)
        close_stream(&video_st);

    if (have_audio)
        close_stream(&audio_st);

    if (!(fmt->flags & AVFMT_NOFILE))
        /* Close the output file. */
        avio_closep(&outctx->pb);

    /* free the stream */
    avformat_free_context(outctx);

    // avoid leaking
    image.release();
    cvcap.release();

    SDL_CloseAudioDevice(output_dev);
    SDL_CloseAudioDevice(input_dev);

    if (first.joinable())
        first.join();

    if (audioBuffer)
        free(audioBuffer);

    return EXIT_SUCCESS;
}

// Workaround, for testing purpose : verify the INTEGRATED_MUXER constant works
// WITHOUT THAT, when INTEGRATED_MUXER is defined, the following build breakage MUST occur :
// /usr/bin/ld : /usr/lib/gcc/x86_64-linux-gnu/9/../../../../x86_64-linux-gnu/lib/../lib/Scrt1.o : dans la fonction « _start » :
// (.text+0x24) : référence indéfinie vers « main »
// collect2: error: ld returned 1 exit status
#ifdef INTEGRATED_MUXER
int main(int argc, char **argv)
{
   muxer_main(argc, argv);

   return EXIT_SUCCESS;
}
#endif

