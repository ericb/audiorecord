
step 5: record audio and video, and mux them


BE CAREFULL : THIS IS WORK IN PROGRESS. DO NOT TRUST THE CODE, PROVIDED AS IT AND BACKUP YOUR DATA.

Everything is under GPL v2 licence.


The code is linked to miniDart (https://framagit.org/ericb/miniDart) as a part of the project.

In this code, you'll find video creation from a webcam (audio + video)

1) audio callbacks (working)   OK

2) video capture (using OpenCV)    OK

3) **cv:Mat to AVFrame (video) conversion** OK

4) **audio + video recording and muxing** OK (better result when using .avi as container, or .mkv).

Known issue : when using .mp4 is not correct yet (reverb, noise, and loud sound).

5) a script for building under Linux (should work on Mac OS X)   OK

To test :

- install dependancies : OpenCV (3.4.17 prefered), SDL2 (at least SDL2-2.0.9), libavformat, libavutil, libavcodec, and so on -ffmpeg 4.4.2-)
- connect a webcam, e.g. Logitech C920, and check whether it is seen as /dev/video2. If NOT, please put the right device number at line 43 in muxer.h
- verify compile.sh is executable. If NOT, simply type : chmod ug+x compile.sh in the directory containing the script
- in step6 directory, type :

./compile.sh

If nothing wrong occurs, make sure the webcam is connected, (supposing it is seen as /dev/video${i})  (supposing you verified the i value  before), and start recodring using: 

./muxer afilename.avi  

( or anyFileName.mkv or even filename.mp4, ffmpeg will select the right encoder on the fly)


**IMPORTANT : to stop the recording, hit ESC (escape) KEY**

The final vidéo is around 30 MB per minute (hd720p@24 fps, but you can easely change for 30 fps).

Dependencies are explained in the files (mostly SDL2, OpenCV, ffmpeg API).






