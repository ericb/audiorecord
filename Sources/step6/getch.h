/* Header file getch.h
 * Author : Eric Bachard / sunday 2016, march 27th, 13:24:52 (UTC+0200)
 * This document is under GPL v2 License. See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef __GETCH__H__
#define __GETCH__H__

char getch(void);

#endif /* __GETCH__H__ */
