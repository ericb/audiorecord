
/*
   This file (muxer.cpp) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v2 License.
   Author : Eric Bachard
   Date : 2023/07/06 23:49:47
*/

#ifndef __MUXER_H
#define __MUXER_H

#include "getch.h"

extern "C"
{
    #ifndef __STDC_CONSTANT_MACROS
    #define __STDC_CONSTANT_MACROS
    #endif
    #include <libavutil/error.h>
    #include <libavutil/opt.h>
    #include <libavutil/timestamp.h>
    #include <libavutil/avassert.h>
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include "libavutil/audio_fifo.h"
    #include <libavutil/imgutils.h>
    #include <libavutil/pixdesc.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
}

#ifndef INTEGRATED_MUXER
#define MAIN  main
#else
#define MAIN  muxer_main
#endif

// default video dev is /dev/video2 on Linux
// (external webcam only is used : for privacy reasons the internal webcam isn't used)
// default is supposed to be 0 on both Linux and Windows

#define LINUX_DEFAULT_VIDEO_DEVICE    2
#define WINDOWS_DEFAULT_VIDEO_DEVICE  0

#if defined(Linux)
 #define DEFAULT_VIDEO_DEVICE LINUX_DEFAULT_VIDEO_DEVICE
#elif defined(_WIN32)
 #define DEFAULT_VIDEO_DEVICE WINDOWS_DEFAULT_VIDEO_DEVICE
#else
 // what else ?
 #define DEFAULT_VIDEO_DEVICE WINDOWS_DEFAULT_VIDEO_DEVICE
#endif

#define FRAME_WIDTH  1280
#define FRAME_HEIGHT  720

// max recording time = 1 hour (change for whatever value who will fit your need)
// other way to stop recording immediately is to hit the "ESC" key.
#define STREAM_DURATION   3600.0

//#define AUDIO_SAMPLES_NUMBER 512
//#define AUDIO_SAMPLES_NUMBER 1024
#define AUDIO_SAMPLES_NUMBER 2048
//#define AUDIO_SAMPLES_NUMBER 4096

// AV_SAMPLE_FORMAT, can be planar or not
#define AV_SAMPLE_FORMAT  AV_SAMPLE_FMT_S16
//#define AV_SAMPLE_FORMAT  AV_SAMPLE_FMT_S32
//#define AV_SAMPLE_FORMAT  AV_SAMPLE_FMT_F32

#define CHANNELS 2

//#define DEFAULT_SAMPLE_RATE 44100
#define DEFAULT_SAMPLE_RATE 48000
//#define DEFAULT_SAMPLE_RATE 96000

// Audio quality
#define LOW_AUDIO_BIT_RATE          64000
#define MEDIUM_AUDIO_BIT_RATE       96000
#define HIGH_AUDIO_BIT_RATE         192000
#define VERY_HIGH_AUDIO_BIT_RATE    384000

// Audio default
#define AUDIO_BIT_RATE HIGH_AUDIO_BIT_RATE

// Video quality
#define LOW_VIDEO_BIT_RATE     2000000
#define MEDIUM_VIDEO_BIT_RATE  3000000
#define HIGH_VIDEO_BIT_RATE    4000000

// Video default
#define VIDEO_BIT_RATE MEDIUM_VIDEO_BIT_RATE

#ifdef _WIN32
// Windows default is 30 fps
#define VIDEO_STREAM_FRAME_RATE 30
#elif defined(Linux)
// 24 fps on Linux works well
#define VIDEO_STREAM_FRAME_RATE 24
#else
// other OS : 30 fps
#define VIDEO_STREAM_FRAME_RATE 30
#endif

// default pix_fmt
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P

#define SCALE_FLAGS SWS_BICUBIC


// FIXME FIXME FIXME !!!
// what is the right value ?
// Does multiply by 127.0 have the same effect as >> 8 ?
#define VOLUME_AMPLIFICATION_FACTOR  127.0

//// TESTING ...
//#define SDL_AUDIO_FORMAT AUDIO_F32
//#define SDL_AUDIO_FORMAT AUDIO_F32SYS
//#define SDL_AUDIO_FORMAT AUDIO_S16SYS
//#define SDL_AUDIO_FORMAT AUDIO_S32
//#define SDL_AUDIO_FORMAT AUDIO_S16
#define SDL_AUDIO_FORMAT AUDIO_S16LSB

//#define SDL_AUDIO_BUFFER_SIZE 4096
#define SDL_AUDIO_BUFFER_SIZE 2048
//#define SDL_AUDIO_BUFFER_SIZE 1024
//#define SDL_AUDIO_BUFFER_SIZE 512


////////// AUDIO CALLBACKS STUFF

// Defaut size of the ring buffer is 120000 bytes
#define AUDIO_BUFFER_SIZE   65000

#endif  /* __MUXER_H  */

