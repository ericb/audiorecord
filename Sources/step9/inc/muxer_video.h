/*
   This file (muxer_video.h) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v3 License.
   Author : Eric Bachard
   Date : 2023/07/06 23:49:47
*/

#ifndef __MUXER_VIDEO_H
#define __MUXER_VIDEO_H

extern "C"
{
    #ifndef __STDC_CONSTANT_MACROS
    #define __STDC_CONSTANT_MACROS
    #endif
    #include <libavutil/error.h>
    #include <libavutil/opt.h>
    #include <libavutil/timestamp.h>
    #include <libavutil/avassert.h>
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include "libavutil/audio_fifo.h"
    #include <libavutil/imgutils.h>
    #include <libavutil/samplefmt.h>
    #include <libavutil/pixdesc.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
}

// default video dev is /dev/video2 on Linux
// (external webcam only is used : for privacy reasons the internal webcam isn't used)
// default is supposed to be 0 on both Linux and Windows

#define LINUX_DEFAULT_VIDEO_DEVICE    0
#define WINDOWS_DEFAULT_VIDEO_DEVICE  0

#if defined(Linux)
 #define DEFAULT_VIDEO_DEVICE LINUX_DEFAULT_VIDEO_DEVICE
#elif defined(_WIN32)
 #define DEFAULT_VIDEO_DEVICE WINDOWS_DEFAULT_VIDEO_DEVICE
#else
 // what else ?
 #define DEFAULT_VIDEO_DEVICE WINDOWS_DEFAULT_VIDEO_DEVICE
#endif

// uncomment if your machine seems to not be able to encode 1280x720@24 fps
//#define LOW_RESOLUTION
#ifdef LOW_RESOLUTION
#define FRAME_WIDTH  940
#define FRAME_HEIGHT  560
#else
#define FRAME_WIDTH  1280
#define FRAME_HEIGHT  720
#endif

// IF THE SOUND IS ACCELERATED AND THE LIGHT LOW, JUST CHANGE 
// VIDEO_STREAM_FRAME_RATE value for 15 and it should work as expected
// other trick : enable the webcam backlight compensation if the access
// of this parameter is available

#ifdef _WIN32
// Windows default is 30 fps
#define VIDEO_STREAM_FRAME_RATE 30
#elif defined(Linux)
// 24 fps on Linux works well
#define VIDEO_STREAM_FRAME_RATE 15
#else
// other OS : 30 fps
#define VIDEO_STREAM_FRAME_RATE 30
#endif

// Video quality
#define LOW_VIDEO_BIT_RATE     2000000
#define MEDIUM_VIDEO_BIT_RATE  3000000
#define HIGH_VIDEO_BIT_RATE    4000000

// Video default quality
#define VIDEO_BIT_RATE MEDIUM_VIDEO_BIT_RATE

// default pix_fmt
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P
#define SCALE_FLAGS SWS_BICUBIC

#endif  /* __MUXER_VIDEO_H  */

