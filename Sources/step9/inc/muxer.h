/*
   This file (muxer.h) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v3 License.
   Author : Eric Bachard
   Date : 2023/07/06 23:49:47
*/

#ifndef __MUXER_H
#define __MUXER_H

#include "getch.h"
#include "muxer_video.h"
#include "muxer_audio.h"
#include "muxer_extensions.h"

#ifdef __cplusplus
extern "C"
{
#endif
    #ifndef __STDC_CONSTANT_MACROS
    #define __STDC_CONSTANT_MACROS
    #endif
    #include <libavutil/error.h>
    #include <libavutil/opt.h>
    #include <libavutil/timestamp.h>
    #include <libavutil/avassert.h>
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include "libavutil/audio_fifo.h"
    #include <libavutil/imgutils.h>
    #include <libavutil/samplefmt.h>
    #include <libavutil/pixdesc.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
#ifdef __cplusplus
}
#endif

#ifndef __cplusplus
    typedef uint8_t bool;
    #define true 1
    #define false 0
#endif

// What follows replaces av_err2str, av_ts2str and av_ts2timestr:
static char a[AV_ERROR_MAX_STRING_SIZE] = { 0 };

// ericb 2022/12/31 ALL of those macros below are broken.
// Using some workaround, adapted from :
// https://www.mail-archive.com/ffmpeg-user@ffmpeg.org/msg30270.html
#undef av_err2str
#define av_err2str(errnum) av_make_error_string( \
        (char*)__builtin_alloca(AV_ERROR_MAX_STRING_SIZE), \
        AV_ERROR_MAX_STRING_SIZE, errnum)
// Same fixes for the other macros causing build breakage
#undef av_ts2str
#define av_ts2str(ts) av_ts_make_string(a, ts)

#undef av_ts2timestr
#define av_ts2timestr(ts,tb) av_ts_make_time_string(a, ts, tb)


#ifdef __cplusplus
    #define REINTERPRET_CAST(type, variable) reinterpret_cast<type>(variable)
    #define STATIC_CAST(type, variable) static_cast<type>(variable)
#else
    #define C_CAST(type, variable) ((type)variable)
    #define REINTERPRET_CAST(type, variable) C_CAST(type, variable)
    #define STATIC_CAST(type, variable) C_CAST(type, variable)
#endif

#ifndef INTEGRATED_MUXER
#define MAIN  main
#else
#define MAIN  muxer_main
#endif


#endif  /* __MUXER_H  */

