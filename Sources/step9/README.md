
DONE at step 7: record audio and video, mux them and improve the sound

TODO at step 8 :

- [x] Add a Makefile and reorganize the files
- [x] Improve the Makefile
- [x] Tested : malloc or arrays (new / delete []). Can be tested using -DTESTING_ARRAYS at build time
- [x] test SDL_DequeueAudio()
- [x] crackling is quasi nulled with SDL_DequeueAudio()
- [x] split headers and separate audio / video / extensions
- [x] use frame->data[0] and frame->data[1] for R/L channels
- [ ] convert (ALSA) PCM_S16_LE (SDL) into AUDIO_S16, and AUDIO_S16 into (ffmpeg) AV_FRAME_FMT_S16
- [ ] improve ALSA -> SDL  ->  ffmpeg path for audio data, and control better the process (WIP)
- [x] allow s32p audio sample format (fltp is currently the default)
- [ ] make f32 audio work  (WIP)
- [x] make u8 audio work
- [ ] make s16 audio format work (WIP : sound is recorded, but issue converting s16 into int remains)
- [x] make planar (both s32p and fltp) data storage work for all containers (should work automaticaly)
- [x] no echo / fuzzy sound (8 bits) : was caused by wrong conversion/
- [ ] optimize other parameters
- [x] improve the sound quality
- [x] improve the sample computation when filling in the frame (in get_audio_frame())
- [ ] make .webm work ?


BE CAREFULL : THIS IS WORK IN PROGRESS. DO NOT TRUST THE CODE, PROVIDED AS IT AND BACKUP YOUR DATA.

Everything is under GPL v3 licence.


The code is linked to miniDart (https://framagit.org/ericb/miniDart) as a part of the project.

In this code, you'll find video creation from a webcam (audio + video)

1) **SDL2 audio SDL_DequeueAudio** (working well)   OK

2) video capture (using OpenCV)    OK

3) **cv:Mat to AVFrame (video) conversion** OK

4) **audio + video recording and muxing** OK (better result when using .avi as container, or .mkv).


Known issues : 

- the sound is NOT true stereo, but only duplicated mono R,L
- the sound is not that good, because recorded in 8 bits (U8). WIP to record in AUDIO_S16LE mode (something's going wrong with SDL ALSA initialization)


Linux BUILD only currently, but cross-compilation should work easely. Any feedback is welcome.

**The code is c++17 compliant, compiles and is warnings free using g++-9 and g++-10, on Linux x86_64**

To test :
- **install dependencies** : OpenCV (3.4.17 prefered), SDL2 (at least SDL2-2.0.9), libavformat, libavutil, libavcodec, and so on -ffmpeg 4.4.2-)
- **connect a webcam, e.g. Logitech C920**, and check whether it is seen as /dev/video2.

If the webcam is NOT detected, please READ CAREFULLY BELOW.




Dependencies are :
- OpenCV 3.4.17 (prefered) or 4.6.0 + all headers
- SDL2 + headers
- pkg-config system
- freetype + OpenGL (optionnal)
- libpthread + headers
- libstdc++ + headers (vector, iostream, thread ...)
- libdl

everything ffmpeg (libav C implementation) :
- libavcodec + headers (for all libav* and libswresameple too)
- libavutil
- libavformat
- libavfilter
- libswresample

Once all dependencies satisfied, just type make.

If nothing wrong occurs, you should find **AVrecord** and **AVrecord_debug** binaries
inside build folder. Make sure the webcam is connected, (supposing it is seen as /dev/video${i},
+ supposing you verified the i value  before). If so, start recording using: 

**./build/AVrecord afilename.avi**

IF YOUR WEBCAM IS NOT FOUND :

try to identify the /dev/video* where your webcam is seen. e.g., it is seen seen as /dev/video2

Then add a second argument to the command line, since you can directly provide the device number at launch time: 

**./build/AVrecord afilename.avi 2**

If it does not work, please, file an issue at framagit.

N.B. : whatever valid filename is authorized, and possible container extensions are :
.avi, .mkv, .mov, .mp4 and .flv. 

Please remind .webm do not work yet (WIP)

The final vidéo is around 30 MB per minute (hd720p@24 fps, but you can easely change for 30 fps).
Dependencies are explained in the files (mainly SDL2, OpenCV, ffmpeg API).

**IMPORTANT : to stop the recording, hit ESC (escape) KEY**


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                                         "
"                     INFORMATION about audio                             "
"                                                                         "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

SDL permet d'utiliser le pilote Alsa dans les conditions suivantes :

16 bits stéréo, little endian, fréquence d'échantillonnage 44,1 kHz  aka  s16_LE
3 periods
period_size = 4096

CHANNELS = 2
CHANNEL_LAYOUT = 3  //   c'est ça qui est bizarre ...



Taille du Buffer ?

Détail d'une frame : 1 frame = 4 otets ([LEFT LSB] [LEFT MSB] [RIGHT LSB] [RIGHT MSB])
Si 1 periode = 4096 FRAMES ça fera 16384 bytes à fournir à 

44,1 kHz sample rate = taux d'échantillonnage. À chaque échantillonnage, on collecte une frame, et chaque frame
contient 4 octets ( 1 valeur du canal R codée sur 2 octets, et une valeur du canal D codée sur 2 octets)

16 bits stéreo à 44100 Hz. Qu'est ce que cela entraîne ?

- 1 échantillon analogique en 16 bits demande 2 octets
- 1 frame représente 1 échantillon analogique de chaque canal (voie à un instant donnée)
"                          = 4 octets (un échantillon R/L pour chaque voie =  2 * 2 octets = 32 bits)

Nombre d'otets (bytes) à fournir à chaque seconde : 

// Bps_rate = (num_channels) * (1 sample in bytes) * (analog_rate)
//          = (1 frame) * (analog_rate) = ( 2 channels ) * (2 bytes/sample) * (44100 samples/sec)
//          = 2*2*44100 = 176400 Bytes/sec 

L'initialisation d'Alsa rapporte qu'il y a 3 periodes de taille 4096 frames

IMPORTANT : ce sont des frames, donc à multiplier par 4 en octets)

=> 12288 frames x 4 octets = 49152 octets disponibles à chaque fois, entre 2 interruptions, soit toutes les 0.33 s
Taille minimale du buffer = 2 period_size = 8192 frames = 8192 * 4 = 32768 octets 




Now, if ALSA would interrupt each second, asking for bytes - we'd need to have 176400 bytes ready for it (at end of each second), in order to sustain
analog 16-bit stereo @ 44.1Khz.
▪ If it would interrupt each half a second, correspondingly for the same stream we'd need 176400/2 = 88200 bytes
ready, at each interrupt;
▪ if the interrupt hits each 100 ms, we'd need to have 176400*(0.1/1) = 17640 bytes ready, at each interrupt.
We can control when this PCM interrupt is generated, by seing a period size, which is set in frames.


Si 1 periode = 4096 FRAMES ça fera 16384 bytes 


Now, if ALSA would interrupt each second, asking for bytes - we'd need to have 176400 bytes ready for it (at end of each second), in order to sustain
analog 16-bit stereo @ 44.1Khz.
▪ If it would interrupt each half a second, correspondingly for the same stream we'd need 176400/2 = 88200 bytes
ready, at each interrupt;
▪ if the interrupt hits each 100 ms, we'd need to have 176400*(0.1/1) = 17640 bytes ready, at each interrupt.
We can control when this PCM interrupt is generated, by seing a period size, which is set in frames.



La taille recommandée du buffer est 2 périodes = 32768 octets (ou bytes)

Every frame 







