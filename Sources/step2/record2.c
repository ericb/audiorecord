/*
 * record2.c
 * Copyright Eric Bachard  2020 August 20th / Licence GPL v3
 * This file is under GPL v3 license
 */

// Thanks to stackoverflow website, extremely usefull here !
// SOURCE : https://stackoverflow.com/questions/42990071/recording-microphone-with-sdl2-gets-delayed-by-2-seconds

#include <SDL2/SDL.h>

// maximum recording time (2s)
const int MAX_RECORDING_SECONDS = 2;

#define RECORDING_BUFFER_SECONDS (MAX_RECORDING_SECONDS + 1)

Uint8 * gRecordingBuffer = NULL;

unsigned int gBufferByteSize = 0;
unsigned int gBufferBytePosition = 0;
unsigned int gBufferByteMaxPosition = 0;

void audioRecordingCallback(void *userdata, Uint8 *stream, int len)
{
    fprintf(stderr, "Using callback at %u\n", SDL_GetTicks());

    // capture the sound, then send it to the audio stream who will
    // be multiplexed with frames/images to create the final video

    // Notice the initial position : should be zero
    // Other possible case : we are at the end of the buffer, and we 
    // did not stop to record. So we must return at zero (swap the buffers)
    // --> step 3 or maybe 4 will describe how to achieve that.
    fprintf(stderr, "gBufferBytePosition = %d \n", gBufferBytePosition);

    // Copy audio from stream (source) into the buffer (result)
    memcpy( &gRecordingBuffer[gBufferBytePosition], stream, len);

    // Move along buffer : we added sound, and we'll add more next turn.
    // In parallel, we'll need to copy all the sound previously written
    // that's the reason why we need to know the current bufferbyte position
    gBufferBytePosition += len;

    // add everything there
    // mainly : copy the buffer content into the audio stream
    // who will be sent to the muxer, to create the final video
}

int main()
{
    SDL_Init(SDL_INIT_AUDIO);

    SDL_AudioDeviceID dev;
    SDL_AudioSpec want, have;

    SDL_zero(want);
    SDL_zero(have);

    want.freq = 44100;
    want.format = AUDIO_S32; // AUDIO_S16SYS
    want.channels = 2; // 1;
    want.samples = 4096;
    want.callback = audioRecordingCallback;
    dev = SDL_OpenAudioDevice(SDL_GetAudioDeviceName(0, 1), 1, &want, &have, 0);


    if (have.format != want.format)
    {
        SDL_Log("We didn't get the wanted format.");
        return 1;
    }


    // start recording
    SDL_PauseAudioDevice(dev, 0);

    if (dev == 0)
    {
        SDL_Log("Failed to open audio: %s", SDL_GetError());
        return 1;
    }
    else
    {
        //Calculate per sample bytes
        int bytesPerSample = have.channels * ( SDL_AUDIO_BITSIZE( have.format ) / 8 );

        //Calculate bytes per second
        int bytesPerSecond = have.freq * bytesPerSample;

        //Calculate buffer size
        gBufferByteSize = RECORDING_BUFFER_SECONDS * bytesPerSecond;

        //Calculate max buffer use
        gBufferByteMaxPosition = MAX_RECORDING_SECONDS * bytesPerSecond;

        //Allocate and initialize byte buffer
        gRecordingBuffer = new Uint8[ gBufferByteSize ];
        memset( gRecordingBuffer, 0, gBufferByteSize );
    }


    fprintf(stdout, "Started at %u\n", SDL_GetTicks());

    // could be a while loop, but if we go over the buffer size, we must return at address zero of the buffer
    SDL_Delay(MAX_RECORDING_SECONDS * 1000);

    // the recording continues until the (able to record) device is closed
    delete [] gRecordingBuffer;
    SDL_CloseAudioDevice(dev);
    fprintf( stderr, "gRecordingBuffer = %p \n", gRecordingBuffer);
}


