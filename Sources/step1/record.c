/* record.c
 * Copyright Eric Bachard  2020 August 20th / Licence GPL v3
 * This file is under GPL v3 license
 */

// Thanks to stackoverflow website, extremely usefull here !
// SOURCE : https://stackoverflow.com/questions/42990071/recording-microphone-with-sdl2-gets-delayed-by-2-seconds

#include <SDL2/SDL.h>


// Audio recording with SDL2
// pre-requisite : compile a command line application 
// link to sdl2 using sdl2-config
// Be familiar with SDL2 API, currently:
// - know what an SDL_AudioSpec is
// - know how to use SDL_OpenAudioDevice()

// Step 1 : understand how works a callback, and use one to discover the mechanism

// my first callback
// Acts as a sort of function, used as a pointer (very fast and efficient)

void audioRecordingCallback(void *userdata, Uint8 *stream, int len)
{
    fprintf(stderr, "Using callback at %u\n", SDL_GetTicks());

    // we capture the sound (the way data is stored is not known yet)
    // once captured, the sound will be sent to the audio stream, and will be multiplexed
    // with images to create the final video (some synchronization will be needed)

    // add everything there
    // mainly : copy the buffer content into the audio stream
    // who will be sent to the muxer, to create the final video
}



int main() {
    SDL_Init(SDL_INIT_AUDIO);

    SDL_AudioDeviceID dev;
    SDL_AudioSpec want, have;

    SDL_zero(want);
    SDL_zero(have);

    want.freq = 44100;
    want.format = AUDIO_S32; // AUDIO_S16SYS
    want.channels = 2; // 1;
    want.samples = 4096;
    want.callback = audioRecordingCallback;  // used without () means we have "a pointer to a function"

    //    : 0 for default device (probably internal microphone) and 1 for = recordable
    //   As example, if you connect a webcam, try SDL_GetAudioDevice(1,1)
    dev = SDL_OpenAudioDevice(SDL_GetAudioDeviceName(0, 1), 1, &want, &have, 0);


    if (have.format != want.format)
    {
        SDL_Log("We didn't get the wanted format.");
        return 1;
    }


    // start recording
    SDL_PauseAudioDevice(dev, 0);

    if (dev == 0)
    {
        SDL_Log("Failed to open audio: %s", SDL_GetError());
        return 1;
    }

    fprintf(stdout, "Started at %u\n", SDL_GetTicks());

    // could be a while loop, but if we go over the buffer size, we must return at address zero of the buffer
    SDL_Delay(2000);

    // the recording continues until the (able to record) device is closed
    SDL_CloseAudioDevice(dev);
}


