/*
   This file (muxer_audio.h) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v3 License.
   Author : Eric Bachard
   Date : 2023/07/06 23:49:47
*/

#ifndef __MUXER_AUDIO_H
#define __MUXER_AUDIO_H

#include "getch.h"

extern "C"
{
    #ifndef __STDC_CONSTANT_MACROS
    #define __STDC_CONSTANT_MACROS
    #endif
    #include <libavutil/error.h>
    #include <libavutil/opt.h>
    #include <libavutil/timestamp.h>
    #include <libavutil/avassert.h>
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include "libavutil/audio_fifo.h"
    #include <libavutil/imgutils.h>
    #include <libavutil/samplefmt.h>
    #include <libavutil/pixdesc.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
}

// max recording time = 1 hour (change for whatever value who will fit your need)
// other way to stop recording immediately is to hit the "ESC" key.
#define STREAM_DURATION   3600.0

//#define AUDIO_SAMPLES  512
//#define AUDIO_SAMPLES 1024
#define AUDIO_SAMPLES   2048

// TODO : find the best value for all containers (less crackling, no echo ... and so on)
#define MP4_AUDIO_SAMPLES_NUMBER   AUDIO_SAMPLES
#define MKV_AUDIO_SAMPLES_NUMBER   AUDIO_SAMPLES
#define AVI_AUDIO_SAMPLES_NUMBER   AUDIO_SAMPLES
#define MOV_AUDIO_SAMPLES_NUMBER   AUDIO_SAMPLES
#define FLV_AUDIO_SAMPLES_NUMBER   AUDIO_SAMPLES
#define WEBM_AUDIO_SAMPLES_NUMBER  AUDIO_SAMPLES

// AV_SAMPLE_FORMAT, can be planar or not
// expected
#define AV_SAMPLE_FORMAT  AV_SAMPLE_FMT_S16
// obtained value : AV_SAMPLE_FMT_FLTP

// FIXME : should be stereo ?

// default
//#define DEFAULT_SAMPLE_RATE 44100
#define DEFAULT_SAMPLE_RATE 48000

// Audio quality
#define C920_AUDIO_BIT_RATE         32000
#define LOW_AUDIO_BIT_RATE          64000
#define MEDIUM_AUDIO_BIT_RATE       96000
#define HIGH_AUDIO_BIT_RATE         192000
#define VERY_HIGH_AUDIO_BIT_RATE    384000

#define SAMPLE_TYPE uint8_t
#define CHANNELS_COUNT 1
#define VOLUME_AMPLIFICATION_FACTOR    32768
#define AUDIO_BIT_RATE MEDIUM_AUDIO_BIT_RATE
#define SDL_AUDIO_FORMAT AUDIO_U8

// Testing several cases with AV_SAMPLE_CASE
// EXISTING CASES are :
// AV_SAMPLE_FMT_S16 = 1
// AV_SAMPLE_FMT_S32 = 2
// AV_SAMPLE_FMT_FLT = 3
// AV_SAMPLE_FMT_S16P = 6
// AV_SAMPLE_FMT_S32P = 7
// AV_SAMPLE_FMT_FLTP = 8
// AV_SAMPLE_FMT_S64 = 10
// AV_SAMPLE_FMT_S64P = 11

#define AV_SAMPLE_CASE AV_SAMPLE_FMT_S16

// default (and better results) are for .avi
#define DEFAULT_AUDIO_SAMPLES_NUMBER AUDIO_SAMPLES * CHANNELS_COUNT

////////// AUDIO CALLBACKS STUFF
// frames sizes for every tested containers :
// mkv  : 1536
// avi  : 1152
// flv  : 1152 
// mov  : 1024
// mp4  : 1024
// webm : broken
// Defaut size of the ring buffer is 4000 bytes
// PPCM = 9216 = 9 x 1024 = 6 x 1536 = 8 x 1152
#define AUDIO_BUFFER_SIZE   8192
//#define AUDIO_BUFFER_SIZE  16384

#define  ONE_BYTE_SAMPLE     1
#define  TWO_BYTES_SAMPLE    2
#define  FOUR_BYTES_SAMPLE   4
#define  EIGHT_BYTES_SAMPLE  8

#endif  /* __MUXER_AUDIO_H  */
