
step 7: record audio and video, mux them and improve the sound

TODO: fix remaining issues

- [x] Fixed callback issues with write_pos and read_pos pointers. Seems to work correctly now
- [x] available containers: .avi and .mkv (prefered), or .mp4, .mov, .flv.  NOT WORKING : .webm, caused by strange opus issue
- [x] decrease crackling (not perfect, but far better)
- [x] add .flv container extension (works)
- [x] improved (not fixed yet) .mp4 uggly sound
- [x] full scale sound 
- [x] add .mov container format
- [x] optimize the buffer size + audio samples number, depending on the case (.avi or .mkv or .mp4)
- [x] Choose 2048 samples per audio frame, to avoid crackling
- [x] Choose 16384 bytes for audioBuffer =  integer times the samples size helped to fix read_pos and write_pos loop.
- [x] Add delay to take the audioBuffer size in account (bigger audioBuffer, bigger delay)
- [x] (Workaround) : create a perfectly synchronized video (.avi and .mkv are the best choises)
- [x] Tested : malloc or arrays (new / delete []). Can be tested using -DTESTING_ARRAYS at build time


Future step 8 :

- [ ] test SDL_DequeueAudio()
- [ ] no echo / fuzzy sound
- [ ] optimize other parameters
- [ ] improve the sound quality
- [x] improve the sample computation when filling in the frame (in get_audio_frame())
- [ ] make .webm work ?


BE CAREFULL : THIS IS WORK IN PROGRESS. DO NOT TRUST THE CODE, PROVIDED AS IT AND BACKUP YOUR DATA.

Everything is under GPL v3 licence.


The code is linked to miniDart (https://framagit.org/ericb/miniDart) as a part of the project.

In this code, you'll find video creation from a webcam (audio + video)

1) audio callbacks (working)   OK

2) video capture (using OpenCV)    OK

3) **cv:Mat to AVFrame (video) conversion** OK

4) **audio + video recording and muxing** OK (better result when using .avi as container, or .mkv).



Known issues : 

- the sound is NOT true stereo, but only duplicated mono R,L
- (FIXME) the sound is not that good, probably due to a wrong use of fltp format (planar is RRRR LLLL and the current sound is RLRLRLRL
- when using .flv container, the synchronization is not good  (reverb, noise, and loud sound).
- sound is not perfect. Probably caused by bad use of fltp format.

5) a script for building under Linux (should work on Mac OS X)   OK

To test :

- install dependencies : OpenCV (3.4.17 prefered), SDL2 (at least SDL2-2.0.9), libavformat, libavutil, libavcodec, and so on -ffmpeg 4.4.2-)
- connect a webcam, e.g. Logitech C920, and check whether it is seen as /dev/video2. If NOT, please put the right device number at line 43 in muxer.h
- verify compile.sh is executable. If NOT, simply type : chmod ug+x compile.sh in the directory containing the script
- in step6 directory, type :

./compile.sh

If nothing wrong occurs, make sure the webcam is connected, (supposing it is seen as /dev/video${i})  (supposing you verified the i value  before), and start recodring using: 

./muxer afilename.avi  

( or anyFileName.mkv or even filename.mp4, ffmpeg will select the right encoder on the fly)

The final vidéo is around 30 MB per minute (hd720p@24 fps, but you can easely change for 30 fps).
Dependencies are explained in the files (mostly SDL2, OpenCV, ffmpeg API).


**IMPORTANT : to stop the recording, hit ESC (escape) KEY**

Last but not least : if ffmpeg exists in the $PATH, TWO FILES are created :

- filename.ext (ext is either .avi, .mkv, .mp4, .mov or .flv). This file is not audio/video sync'ed. 
- the second file, named filename_sync.ext IS audio/video synhronized (not perfect but looks promising)

N.B. : the crackling is limited but not perfectly eliminated (WIP)








