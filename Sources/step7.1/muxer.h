/*
   This file (muxer.h) belongs to miniDart project (https://framagit.org/ericb/miniDart)
   under GPL v3 License.
   Author : Eric Bachard
   Date : 2023/07/06 23:49:47
*/

#ifndef __MUXER_H
#define __MUXER_H

#include "getch.h"

extern "C"
{
    #ifndef __STDC_CONSTANT_MACROS
    #define __STDC_CONSTANT_MACROS
    #endif
    #include <libavutil/error.h>
    #include <libavutil/opt.h>
    #include <libavutil/timestamp.h>
    #include <libavutil/avassert.h>
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include "libavutil/audio_fifo.h"
    #include <libavutil/imgutils.h>
    #include <libavutil/pixdesc.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
}

#ifndef INTEGRATED_MUXER
#define MAIN  main
#else
#define MAIN  muxer_main
#endif

// (external webcam only is used : for privacy reasons the internal webcam isn't used)
// default is supposed to be 0 on both Linux and Windows

// default video dev is /dev/video2 on Linux in my case. Adapt to your needs !

#define LINUX_DEFAULT_VIDEO_DEVICE    2
#define WINDOWS_DEFAULT_VIDEO_DEVICE  0

#if defined(Linux)
 #define DEFAULT_VIDEO_DEVICE LINUX_DEFAULT_VIDEO_DEVICE
#elif defined(_WIN32)
 #define DEFAULT_VIDEO_DEVICE WINDOWS_DEFAULT_VIDEO_DEVICE
#else
 // what else ?
 #define DEFAULT_VIDEO_DEVICE WINDOWS_DEFAULT_VIDEO_DEVICE
#endif

#define FRAME_WIDTH  1280
#define FRAME_HEIGHT  720

// max recording time = 1 hour (change for whatever value who will fit your need)
// other way to stop recording immediately is to hit the "ESC" key.
#define STREAM_DURATION   3600.0

#define SAMPLES 2048
// TODO : find the best value for all containers (less crackling, no echo ... and so on)
#define MP4_AUDIO_SAMPLES_NUMBER  SAMPLES
#define MKV_AUDIO_SAMPLES_NUMBER  SAMPLES
#define AVI_AUDIO_SAMPLES_NUMBER  SAMPLES

// default (and better results) are for .avi
#define DEFAULT_AUDIO_SAMPLES_NUMBER AVI_AUDIO_SAMPLES_NUMBER

// AV_SAMPLE_FORMAT, can be planar or not
#define AV_SAMPLE_FORMAT  AV_SAMPLE_FMT_S16

// FIXME : should be stereo ? SDL2 issue ?
#define CHANNELS_COUNT 1

//#define DEFAULT_SAMPLE_RATE 48000
#define DEFAULT_SAMPLE_RATE 44100

// Audio quality
#define C920_AUDIO_BIT_RATE         32000
#define LOW_AUDIO_BIT_RATE          64000
#define MEDIUM_AUDIO_BIT_RATE       96000
#define HIGH_AUDIO_BIT_RATE         192000
#define VERY_HIGH_AUDIO_BIT_RATE    384000

// Audio default
#define AUDIO_BIT_RATE C920_AUDIO_BIT_RATE

// Video quality
#define LOW_VIDEO_BIT_RATE     2000000
#define MEDIUM_VIDEO_BIT_RATE  3000000
#define HIGH_VIDEO_BIT_RATE    4000000

// Video default
#define VIDEO_BIT_RATE MEDIUM_VIDEO_BIT_RATE

#ifdef _WIN32
// Windows default is 30 fps
#define VIDEO_STREAM_FRAME_RATE 30
#elif defined(Linux)
// 24 fps on Linux works well
#define VIDEO_STREAM_FRAME_RATE 24
#else
// other OS : 30 fps
#define VIDEO_STREAM_FRAME_RATE 30
#endif

// default pix_fmt
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P

#define SCALE_FLAGS SWS_BICUBIC

// TODO : amplify the right value
//#define VOLUME_AMPLIFICATION_FACTOR  16384
#define VOLUME_AMPLIFICATION_FACTOR  32768

#define SDL_AUDIO_FORMAT AUDIO_S16SYS

////////// AUDIO CALLBACKS STUFF
// frames sizes for every tested containers :
// mp4 : 1024, mkv : 1536, avi : 1152
// Defaut size of the ring buffer is 4000 bytes
// PPCM = 9216 = 9 x 1024 = 6 x 1536 = 8 x 1152
#define AUDIO_BUFFER_SIZE  16384
#endif  /* __MUXER_H  */

