#!/bin/bash

#DEBUG=-g
EXTRA_FEATURES=-DDEBUG_CALLBACKS

g++   -Wall -std=c++11 -I/usr/include -I. ${DEBUG} ${EXTRA_FEATURES}\
     -o audio \
     getch.c audio_circ.c \
    `sdl2-config --cflags --libs` -lpthread
