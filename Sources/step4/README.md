step 4 :

Record and playback AUDIO (only)

Still using SDL 

1) create 2 callbacks:
- one for recording audio from the webcam;
- one for playback

2) Using sort if circular buffer, largely inspired from Eric Scrivner work.

See : https://github.com/etscrivner/sdl_audio_circular_buffer 

2) Create a thread, and wait for hiting ESC key to stop

Tested working very well under Linux (do not expect easy porting on Windows).

Not sure the code is that good, nor safe (waiting for experts helping me to improve)

Everything is under GPL license (V3).

Have Fun
Eric Bachard
