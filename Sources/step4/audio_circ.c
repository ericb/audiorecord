/* File audio_circ.c
 * Author : Eric Bachard / sunday 2023, january 15th, 22:13:25 (UTC+0200)
 * This file is under GPL v3 License. See : http://www.gnu.org/licenses/gpl-3.0.html
 */

 /* Many thanks to etscrivner for his code example at
  * https://github.com/etscrivner/sdl_audio_circular_buffer
  * who helped and inspired me 
  */

//   Other ressources, SEE :
//     https://trac.ffmpeg.org/wiki/audio%20types
//     https://wiki.libsdl.org/SDL2/SDL_AudioFormat
//     https://stackoverflow.com/questions/70149960/sdl-audio-capture-does-not-detect-any-audio-when-recording-with-a-microphone
//     https://www.gamedev.net/forums/topic/624876-how-to-read-an-audio-file-with-ffmpeg-in-c/


#include <SDL2/SDL.h>

#include <stdio.h>
#include <stdlib.h>
#include <getch.h>
#include <thread>

// 20480 = 10 * 2048 = 10 * 1024 samples x 2 channels in 16 bits (AUDIO_S16)
// 40960 = 10 * 4096 = 10 * 2048 samples x 2 channels in 32 bits (AUDIO_S16)
#define BUFFER_SIZE   40960

static SDL_AudioDeviceID input_dev;
static SDL_AudioDeviceID output_dev;

static Uint8* buffer    = 0;
static int    write_pos = 0;
static int    read_pos  = 0;
static bool   b_quit    = false;

void cb_in(void *userdata, Uint8 *stream, int len)
{
    if (read_pos - write_pos > 0)
    {
        if (len > (read_pos - write_pos))
            len = (read_pos - write_pos);

#ifdef DEBUG_CALLBACKS
        fprintf(stderr, "read_pos - write_pos > 0, et len = %d \n", len);
#endif
        SDL_memcpy(buffer + write_pos, stream, len);
        write_pos += len;
    }
    else
    {
        if ((write_pos + len) > (BUFFER_SIZE))
        {
            int len1 = BUFFER_SIZE - write_pos;
            int len2 = write_pos + len - BUFFER_SIZE;

            SDL_memcpy(buffer + write_pos, stream, len1);
            SDL_memcpy(buffer, stream, len2);

            write_pos = len2;
        }
        else
        {
            SDL_memcpy(buffer + write_pos, stream, len);
            write_pos = write_pos + len;
        }
    }

#ifdef DEBUG_CALLBACKS
    fprintf(stderr, "IN: len = %d\t write_pos =  %d\t stream contains : %d %d %d %d %d %d %d\n",
            len, write_pos, stream[0], stream[1], stream[2], stream[3], stream[4], stream[5], stream[6]);
#endif
    if (write_pos >= BUFFER_SIZE)
        write_pos %= BUFFER_SIZE;

//        write_pos = write_pos - BUFFER_SIZE;

    // Make sure the thread completes when application quit, to avoid leaking ressources
    if (b_quit == true)
        return;
}

void cb_out(void *userdata, Uint8 *stream, int len)
{
    if (read_pos > write_pos)
    {
        if ((read_pos + len) > BUFFER_SIZE)
        {
            // read the end of the buffer
            int len5 = BUFFER_SIZE - read_pos;
            SDL_memcpy(stream, buffer + read_pos, len5);
            // + the beginning
            int len6 = (read_pos + len) - BUFFER_SIZE;
            SDL_memcpy(stream, buffer, len6);
            read_pos = len6;
        }
        else
        {
            SDL_memcpy(stream, buffer + read_pos, len);
            read_pos += len;
        }
    }
    else if (read_pos + len > write_pos)
    {
        SDL_memcpy(stream, buffer + read_pos, write_pos - read_pos);
        read_pos += (write_pos - read_pos);
    }
    else
    {
        SDL_memcpy(stream, buffer + read_pos, len);
        read_pos += len;
    }

#ifdef DEBUG_CALLBACKS
    fprintf(stderr, "OUT: len = %d\t  read_pos =  %d\t stream contains : %d %d %d %d %d %d %d \n",
            len, read_pos, stream[0], stream[1], stream[2], stream[3], stream[4], stream[5], stream[6]);
#endif
    if (read_pos + len > BUFFER_SIZE)
        read_pos = (read_pos + len)- BUFFER_SIZE;

    // Make sure the thread completes when application quit, to avoid leaking ressources
    if (b_quit == true)
        return;
}


static void keyboard_loop()
{
    char c = 0;
    while (c != 27) // esc
    {
        c = 0;
        c=getch();
    }

    if (c == 27) // esc
        b_quit = true;
}

int main(void) {
    SDL_Init(SDL_INIT_AUDIO);

    buffer = (uint8_t *)malloc(BUFFER_SIZE);

    SDL_AudioSpec want_in, want_out, have_in, have_out;

    SDL_zero(want_out);
    want_out.freq = 44100;
    want_out.format = AUDIO_S32;// AUDIO_S16
    want_out.channels = 2;
    want_out.samples = 1024;
    want_out.callback = cb_out;

#ifdef DEBUG_CALLBACKS
    fprintf(stderr, "Adresse de cb_out =  %p  \n", &cb_out);
#endif
    output_dev = SDL_OpenAudioDevice(NULL /* default */, 0 /* isCapture*/ , &want_out, &have_out, SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (output_dev == 0)
    {
        SDL_Log("Failed to open output: %s", SDL_GetError());
        return 1;
    }

    SDL_zero(want_in);
    want_in.freq = 44100;
    want_in.format = AUDIO_S32;// AUDIO_S16
    want_in.channels = 2;
    want_in.samples = 1024;
    want_in.callback = cb_in;

#ifdef DEBUG_CALLBACKS
    fprintf(stderr, "Adresse de cb_in =  %p  \n", &cb_in);
#endif
    input_dev = SDL_OpenAudioDevice(NULL /* default */, 1 /* isCapture */, &want_in, &have_in, SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (input_dev == 0) {
        SDL_Log("Failed to open input: %s", SDL_GetError());
        return 1;
    }

    SDL_PauseAudioDevice(input_dev, 0);
    SDL_PauseAudioDevice(output_dev, 0);

    std ::thread first(keyboard_loop);

    // IMPORTANT : the webcam continues to read/write audio data
    // until the ESC key is hit AND CTRL+C does not stop the loop !

    if (first.joinable())
        first.join();

    SDL_CloseAudioDevice(output_dev);
    SDL_CloseAudioDevice(input_dev);

    if (buffer)
        free(buffer);
}
