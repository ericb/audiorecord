// A test program to copy audio in (microphone) to audio out (speaker) via SDL.
//

#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 1024

// Stereo float32 samples.
static uint8_t saved[BUF_SIZE*2*sizeof(float)];

// Copies audio callback data into the saved buffer.
void audioReader(void* udata, uint8_t* buf, int len) {
  fprintf(stderr, "Reading audio: %d -> %ld bytes \n", len, sizeof(saved));
  memcpy(saved, buf, len);
}

// Copies saved audio data into the callback buffer.
void audioWriter(void* udata, uint8_t* buf, int len) {
  fprintf(stderr, "Writing audio: %ld -> %d bytes \n", sizeof(saved), len);
  memcpy(buf, saved, len);
}

// List all devices of the given type, and return the name of the first or NULL.
// Caller must free the returned pointer.
char* ChooseDevice(int is_capture) {
  int dev_cnt = SDL_GetNumAudioDevices(is_capture);
  if (dev_cnt < 1) {
    fprintf(stderr, "No %s devices: %s", is_capture ? "capture" : "playback \n", SDL_GetError());
    return NULL;
  }

  printf("%s devices: \n", is_capture ? "capture" : "playback \n");
  char * dev_name = NULL;
  for (int i = 0; i < dev_cnt; i++) {
    printf("%c %s", !dev_name ? '*' : ' ', SDL_GetAudioDeviceName(i, is_capture));
    if (!dev_name) {
      const char* tmp = SDL_GetAudioDeviceName(i, is_capture);
      dev_name = (char *)malloc(strlen(tmp)+1);
      strcpy(dev_name, tmp);
    }
  }

  if (!dev_name) {
    fprintf(stderr, "No %s devicesn", is_capture ? "capture" : "playback \n");
  }
  return dev_name;
}

// Opens and unpauses the first device of the given type, returning its ID, or
// returns 0.
SDL_AudioDeviceID OpenDevice(int is_capture) {
  char* dev_name = ChooseDevice(is_capture);
  if (!dev_name) return 0;

  SDL_AudioSpec spec;
  SDL_memset(&spec, 0, sizeof(spec));
  spec.freq = 48000;
  spec.format = AUDIO_F32;
  spec.channels = 2;
  spec.samples = BUF_SIZE;
  spec.callback = is_capture ? audioReader : audioWriter;

  SDL_AudioDeviceID dev_id = SDL_OpenAudioDevice(dev_name, is_capture, &spec, NULL, 0);
  if (dev_id == 0) {
    fprintf(stderr, "Failed to open %s device %s: %sn", is_capture ? "input" : "output \n", dev_name, SDL_GetError());
    return 0;
  }

  free(dev_name);
  SDL_PauseAudioDevice(dev_id, SDL_FALSE);
  return dev_id;
}

int main(int argc, char** argv) {
  SDL_memset(saved, 0, sizeof(saved));
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    fprintf(stderr, "Failed to initialize SDL audio: %s \n ", SDL_GetError());
    return 1;
  }

  SDL_AudioDeviceID in_dev_id = OpenDevice(/* is_capture = */ SDL_TRUE);
  if (in_dev_id == 0) return 1;

  SDL_AudioDeviceID out_dev_id = OpenDevice(/* is_capture = */ SDL_FALSE);
  if (out_dev_id == 0) return 1;

  SDL_Delay(2000);  // 5 seconds
  SDL_CloseAudioDevice(in_dev_id);
  SDL_CloseAudioDevice(out_dev_id);
  SDL_Quit();
  return 0;
}
